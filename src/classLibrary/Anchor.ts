interface Anchor {
    macAddress: string
    x: number;
    y: number;
    radius: number;
}

interface Tags {
    macAddress: string;
    uuid: string;
    x: number;
    y: number;
    sync: boolean;
}

export {Anchor, Tags}